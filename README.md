# ImgHome

#### 介绍
专门用来做pigGo软件上传图片的图床。

#### 软件架构
当前版本：pigGo2.2.2，typora，markdown


#### 安装教程

1. PicGo下载
在PicGo官方的github地址下载即可，建议选择2.2.0以上版本，支持PicGo-Server配置自定义端口。
[PicGo下载](https://github.com/Molunerfinn/PicGo/releases)，点进去再点击exe下载。

2. Gitee插件安装
安装软件，在插件设置中搜索 gitee(搜索栏区分大小写)，并安装，搜索到的两个插件，选择其中一个都可以。我选的第二个。
安装完毕重新启动，左侧图床设置中将会加入Gitee图床
3. 码云仓库准备
登录码云，新建仓库，如果要于外网访问则选择公开仓库(如果选择私有仓库则在typora软件内部也无法预览)。
点击右上角头像选择设置，在安全设置中找到私人令牌，选择生成新令牌，权限勾选projects第一项即可。

4. PicGo Gitee配置
在PicGo的Gitee图床设置中填写相关内容，
![配置图床](https://images.gitee.com/uploads/images/2020/1118/093355_ddf86abb_1585952.png "屏幕截图.png")

5. Typora图床配置
在Typora中点击文件->偏好设置 选择图片页签(优先使用相对路径的配置比较奇怪，如果粘贴进去的图片是以相对路径展示的并且查找不到，则勾选/取消勾选一下该配置试一试)
![配置typora](https://images.gitee.com/uploads/images/2020/1118/093450_06705c56_1585952.png "屏幕截图.png")

点击验证图片上传选项如果成功则代表配置完成。


6. 测试出现成功连接PicGo但返回失败。

这是由于上传的文件名重复导致，在PicGo软件的设置中开启时间戳重命名功能，或者开启上传时重命名功能即可。

2020-11-18 -zoutao